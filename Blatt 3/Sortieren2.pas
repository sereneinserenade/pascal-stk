program sortieren;
	var
		ar: array[1..100] of longint;
		b, i, n, j : longint;
	
		FUNCTION weniger(a, b: longint) : boolean;
		BEGIN
			weniger:=a < b;
		END;

	begin
		write('Wie viele Zahlen möchten Sie eingeben ?');
		read(n);

		writeln('Geben Sie jetzt die Zahlen ein.');

		for i:=1 to n do
			begin
				writeln(i,'>');
				read(ar[i]);
			end;

		for j:=1 to n do
			begin
				for i:=1 to n-1 do
					begin 
						b := ar[i];
						if weniger(ar[i], ar[i+1]) then
							begin
								ar[i] := ar[i+1];
								ar[i+1] := b;
							end;
					end;
			end;

		writeln;

		write('Alle absteigend sortierte Zahlen sind: ');

		for i:=1 to n do
			write(ar[i], ',');
	writeln;
end.
