{Wählen Sie ein Programm aus dem Übungsblatt 1 oder 2 aus und verändern Sie das Programm, indem Sie Procedures oder Functions verwenden!}

{ hier ist das Program von zahl.pas von Blatt2 }

PROGRAM UEBUNG;
	VAR
		n, a, i, b : longint;

		FUNCTION satz(a,n : longint) : array[1..2] of longint;
			BEGIN
				satz[1]:= a;
				satz[2]:= n - a;
			END;

	BEGIN
		writeln('Geben Sie eine natürliche Zahl ein : ');
		read(n);

		writeln('Die Zahlenpaare sind: ');
		for i:=1 to n-1 do
			BEGIN
				writeln('(', satz(i,n)[1], ',', satz(i,n)[2], ') ');
			END;
		writeln;
	END.
