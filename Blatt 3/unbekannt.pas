{ dieses Program nimmt eine Zahl vom Nutzer und schreibt ein baum mit Blätterbereich von dieser Zahl, und gibt einen Baumstamm von ungefähr Zeile dividiert durch 3 Zeilen } 
PROGRAM unbekannt;
	VAR
		num, k, i, j, n: longint;
	BEGIN
		write('Je nach dem wie größ Sie den Baum haben möchten, geben Sie die Nummer ein : ');  
		readln( n );  { von dem Nutzer die Eingabe nehmen }

		for i:=1 to n do
			BEGIN

				{ die Leerezeichen für linke seite des Baums ohne Blätter zu zeichen. }
				for j:=i to n-1 do
					BEGIN
						write(' ');
					END;

				{ die Sternzeichen für Blätter}
				for k:=1 to i do
					BEGIN
						write('*');
					END;
				if i>1 then
					BEGIN
						for k:=1 to i-1 do
							BEGIN
								write('*');
							END;
					END;
				writeln();
			END;

		{Den Baumstamm zu zeichen.}
		num := n div 3;
		for i:=1 to num do 
			BEGIN
				for j:=1 to n-2 do 
					BEGIN
						write(' ');
					END;
				write('###');
				writeln();
			END;
	END.
