program collatz;
        var numin, count, loopCount: Integer;
        begin
                writeln('Geben Sie eine natürliche Zahl ein: ');
                readln( numin );
                if numin<1 then
                        writeln('Die gegebene Zahl ist nicht natürlich.')
                else
                        begin
                        count := numin;
                                repeat
                                        begin
                                                if (count mod 2)=0 then
                                                        begin
                                                                count := count div 2;
                                                                writeln( count );
                                                       end
                                                else
                                                        begin
                                                                count:= (3 * count) + 1;
                                                                writeln( count );
                                                        end;
                                                loopCount := succ(loopCount)
                                        end
                                until count = 1;
                        end;
                writeln(' The number of iterations are ', loopCount );
end. 
