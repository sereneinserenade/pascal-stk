program maxmin;
	var
		n, i, min, max, x, span, lastcount: integer;
		things : real;
	begin
		write('Geben Sie n ein: ');
		read( n );

		writeln('Jetzt geben Sie n Nummern ein.');
		for i:=1 to n do 
			begin
				read( x );
				lastcount := x;

				if (i=1) then
					begin
						min := lastcount;
						max := lastcount;
					end;
				if ((i>1) and (x>max))=True then
					max := x;
				if ((i>1) and (x<min))=True then
					min := x;
			end;
			
		writeln('Spannweite :', max-min);
		span := max+min;
		things := span/2;
		writeln('Average :', things:0:2);
				
		writeln('max ', max);
		writeln('min ', min);
end.
