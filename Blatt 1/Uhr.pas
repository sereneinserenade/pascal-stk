program Uhr;
         var
               time: Longint; hour: integer; minutes: integer; timesecinbet: integer; timesec: integer;
         Begin
                write('Gib die Zeit in Sekunden ein: ');
                readln ( time );
                hour := time div 3600;
                timesecinbet := time - ( hour * 3600 );
                minutes := timesecinbet div 60;
                timesec := timesecinbet - ( minutes * 60 );
                timesec := timesec div 1;
                if hour<10 then
                        write('0', hour)
                else
                        write( hour );
        write( ':' );
                if minutes<10 then
                        write('0', minutes )
                else
                        write( minutes );
        write( ':' );
                if timesec<10 then
                        writeln('0', timesec )
                else
                        writeln ( timesec, ' ' );
         End.
