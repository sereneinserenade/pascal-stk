Program Fakultaet;
        Var
        num: Longint; count: integer; res: longint;
        Begin
                write(' Gib eine Zahl zur Rechnung von fakultät: ');
                read( num );
                count := num;
                res := 1;
                if num < 0 then
                        writeln (' Fakultät von negativen Zahlen existiert nicht !' )
                else if num=0 then writeln('Fakultät von 0 ist 1')
                else
                        Begin
                                Repeat
                                        Begin
                                                res := res * count;
                                                count := pred( count )
                                        End
                                Until count < 1;
                                writeln( res, ' ist die antwort von ', num, '!' );
                        End
        End.
