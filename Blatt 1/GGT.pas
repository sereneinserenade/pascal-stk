program ggt;
        var
                num1, num2, count: longint;
        begin
                write('Geben Sie eine natürliche Zahl ein: ');
                read(num1);
                write('Geben Sie zweite natürliche Zahl ein: ');
                read(num2);
                if ( num1 or num2) < 0 then
                        writeln('Nicht natürliche Zahlen.')
                else
                        if num1>num2 then
                                begin
                                        count := num2;
                                        while ((num1 mod count)=0) and ((num2 mod count)=0) = false do
                                                count:= count - 1 ;
                                                writeln( count );
                                end
                        else
                                begin
                                        count := num1;
                                        while ((num2 mod count)=0) and ((num1 mod count)=0) = false do
                                                count:= count - 1 ;
                                                writeln( count );
                                end;

        end.
