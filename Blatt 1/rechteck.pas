program rechteck;
	var
		umf, fla, dia, a, b: real;

	begin
		writeln('Geben Sie bitte Seitenlänge eines Rechtecks, Sie müssen Sie als a und b eingeben. Geben Sie a : ');
		read(a);
		writeln('Geben SIe b : ');
		read(b);
		umf := 2 * ( a + b);
		fla := a * b;
		dia := sqrt( sqr(a) + sqr(b));
		writeln;
		writeln('Umfang dieses Rechtecks ist ', umf:0:1, ' Einheit, Fläche ist ', fla:0:1, ' Einheit^2, Länge des Diagonals ist ', dia:0:1, ' Einheit.');

end.
