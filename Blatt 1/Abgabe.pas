program maxmin;
	var
		n, i, min, max, x, span, lastcount: integer;
		things : real;
	begin
		write('Geben Sie n ein: ');
		read( n );

		writeln('Jetzt geben Sie n Nummern ein.');
		for i:=1 to n do 
			begin
				read( x );
				lastcount := x;

				if (i=1) then
					begin
						min := lastcount;
						max := lastcount;
					end;
				if ((i>1) and (x>max))=True then
					max := x;
				if ((i>1) and (x<min))=True then
					min := x;
			end;
			
		writeln('Spannweite :', max-min);
		span := max+min;
		things := span/2;
		writeln('Average :', things:0:2);
				
		writeln('max ', max);
		writeln('min ', min);
end.
program bin;
	var
		n, out, bet, b : longint;
	begin
		write('Geben Sie eine positive Nummer ein: ');
		read(n);
		
		if n>0 then
			begin 
				out := 0;
				bet := 1;
				while n <> 0 do 
					begin
						b := n mod 2;
						n := n div 2;
						out := b*bet+ out;
						bet := 10*bet;
					end;
				writeln(out);
			end
		else 
			writeln('Geben Sie eine positive Nummer ein. ');
end.
