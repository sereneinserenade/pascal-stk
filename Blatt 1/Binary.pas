program bin;
	var
		n, out, bet, b : longint;
	begin
		write('Geben Sie eine positive Nummer ein: ');
		read(n);
		
		if n>0 then
			begin 
				out := 0;
				bet := 1;
				while n <> 0 do 
					begin
						b := n mod 2;
						n := n div 2;
						out := b*bet+ out;
						bet := 10*bet;
					end;
				writeln(out);
			end
		else 
			writeln('Geben Sie eine positive Nummer ein. ');
end.
