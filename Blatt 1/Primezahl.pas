program primezahl;
        var
                num, did, count: longint;
        begin
                writeln('Geben Sie die Nummer ein, behalten Sie bitte im Kopf, dass wenn eine negative Zahl gegeben wird, wird der Betrag von der Zahl zur Prüfung genommen.');
                readln( num );
                num := abs( num );
                did := 1;
                count := 0;
                if (num <> 0) then
                        begin
                                while (did < num) do
                                        begin
                                                if (num mod did)=0 then
                                                        count := count + 1
                                                else;
                                                did := did + 1
                                        end;
                        if ((count = 2) or (count > 2)) then
                                writeln( 'Die gegebene Zahl ist eine Primzahl.')
                        else
                                writeln( 'Die gegebene Zahl ist keine Primzahl.')
                        end
                else
                        writeln('Die zahl ist null.');
                end.
