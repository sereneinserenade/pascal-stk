program Gleichung;
	var 
		a, b, c, d, wurzel1, wurzel2 : real;
	begin
		writeln('Geben Sie die Koeffizienten einer queadratischen Gleichung ein: ');
		read( a, b, c);
		d := ((b*b) - (4*a*c));

		if (d<0) then
			begin 
				writeln('Die wurzel dieser Gleichugn sind imaginär ( nicht möglich sozusagen )');
			end
		else
			begin
				wurzel1 := (((-b)+(sqrt(d)))/(2*a));
				wurzel2 := (((-b)-(sqrt(d)))/(2*a));
			end;
	
		if ( wurzel1 = wurzel2 ) then
			writeln('Die wurzel dieser Gleichung sing gleich und sie betragen ', wurzel1:0:2)
		else 
			writeln('Die Wurzel dieser Gleichung sind ', wurzel1:0:2, ' und ', wurzel2:0:2);
end.
