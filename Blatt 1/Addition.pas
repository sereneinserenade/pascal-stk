Program Addition;
Var
        x,y,z: Real;
Begin
        Writeln('Geben Sie zwei ganze Zahlen ein: ');
        read(x,y);

        z:=x+y;
        Writeln(x, '+', y, '= ', z);

        z:=x-y;
        Writeln(x, '-', y, '= ', z);

        z:=x*y;
        Writeln(x, '*', y, '= ', z);

        z:=x/y;
        Writeln(x, '/', y, '= ', z);
End.
