program vector;
        var
		x1, x2, y1, y2, z1, z2, sprod, vprodx, vprody, vprodz : real;

        begin
                write('Geben Sie ersten Vector: ');
                readln( x1, y1, z1);
                write('Geben Sie zweiten Vector: ');
                readln( x2, y2, z2);

                sprod := ( x1*x2 + y1*y2 + z1*z2);
		writeln('S Produkt ist ', sprod:0:2);

		vprodx := (( y1 * z2 ) - ( y2 - z1));
		vprody := (( z1 * x2 ) - ( z2 - x1));
		vprodz := (( x1 * y2 ) - ( x2 - y1));
		writeln('Vektor Produkt ist ', vprodx:0:2, 'i+', vprody:0:2, 'j+', vprodz:0:2, 'k');
        end.

