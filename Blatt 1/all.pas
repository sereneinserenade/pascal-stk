program ableitung;
	var
		a, b, c: integer;
	begin
		write('Geben Sie a, b, c');
		read(a, b, c);

		writeln('Die Ableitung ist: ', 2*a, 'x + ', b);
	end.
Program Addition;
Var
        x,y,z: Real;
Begin
        Writeln('Geben Sie zwei ganze Zahlen ein: ');
        read(x,y);

        z:=x+y;
        Writeln(x, '+', y, '= ', z);

        z:=x-y;
        Writeln(x, '-', y, '= ', z);

        z:=x*y;
        Writeln(x, '*', y, '= ', z);

        z:=x/y;
        Writeln(x, '/', y, '= ', z);
End.
program Anfangsbuchstabe;
        var
                firstname: string; lastname: string;
        Begin
                write('Gib deinen Vornamen ein: ');
                readln ( firstname );
                write('Gib denen Namen ein: ');
                readln ( lastname );
                writeln( ' Der Name beginnt mit: ', lastname[1]);
                writeln( ' Der Vorname beginnt mit: ', firstname[1]);
        End.
program bin;
	var
		n, bnum, pot, b : longint;
	begin
		write('Geben Sie eine positive Nummer ein: ');
		read(n);
		
		if n>0 then
			begin 
				bnum := 0;
				pot := 1;
				while n <> 0 do 
					begin
						b := n mod 2;
						n := n div 2;
						bnum := b*pot + bnum;
						pot := 10*pot;
					end;
				writeln(bnum);
			end
		else 
			writeln('Geben Sie eine positive Nummer ein. ');
end.
program collatz;
        var numin, count, loopCount: Integer;
        begin
                writeln('Geben Sie eine natürliche Zahl ein: ');
                readln( numin );
                if numin<1 then
                        writeln('Die gegebene Zahl ist nicht natürlich.')
                else
                        begin
                        count := numin;
                                repeat
                                        begin
                                                if (count mod 2)=0 then
                                                        begin
                                                                count := count div 2;
                                                                writeln( count );
                                                       end
                                                else
                                                        begin
                                                                count:= (3 * count) + 1;
                                                                writeln( count );
                                                        end;
                                                loopCount := succ(loopCount)
                                        end
                                until count = 1;
                        end;
                writeln(' The number of iterations are ', loopCount );
end. 
program einkaufen;
        var
                geld, cost, count, geld1, geld2, geldbet: real;
        begin
                write('Wie viel Geld haben Sie?');
                readln( geld );
                write('Wie viel kostet ein Ananas?');
                readln( cost );

                if ((geld > 0) or (geld=0)) and ( (cost >0) or (cost<0) )then
                        begin
                                count := geld/cost;
                                count := trunc( count );
                                geldbet := geld - (cost*count);
                                geld1 := trunc(geldbet);
                                geld2 := geldbet - geld1;
                                geld2 := geld2 * 100;

                                writeln('Sie können ', count:0:0, ' Ananas kaufen.');
                                writeln('Sie haben ', geld1:0:0, ',', geld2:0:0, ' übrig.');
                        end
                else
                        writeln('Geben Sie korrekte Nummer ein.')
        end.

Program Fakultaet;
        Var
        num: Longint; count: integer; res: longint;
        Begin
                write(' Gib eine Zahl zur Rechnung von fakultät: ');
                read( num );
                count := num;
                res := 1;
                if num < 0 then
                        writeln (' Fakultät von negativen Zahlen existiert nicht !' )
                else if num=0 then writeln('Fakultät von 0 ist 1')
                else
                        Begin
                                Repeat
                                        Begin
                                                res := res * count;
                                                count := pred( count )
                                        End
                                Until count < 1;
                                writeln( res, ' ist die antwort von ', num, '!' );
                        End
        End.
program Fibonacci;
        var
                a,b,numbet,count : longint;
        begin
        a := 0;
        b := 1;
        count := 2;
        write(a, ',', b, ',');
        repeat
                numbet := a + b;
                write( numbet , ', ');
                a := b;
                b := numbet;
                count := succ( count );
        until count=30;
end.
program ggt;
        var
                num1, num2, count: longint;
        begin
                write('Geben Sie eine natürliche Zahl ein: ');
                read(num1);
                write('Geben Sie zweite natürliche Zahl ein: ');
                read(num2);
                if ( num1 or num2) < 0 then
                        writeln('Nicht natürliche Zahlen.')
                else
                        if num1>num2 then
                                begin
                                        count := num2;
                                        while ((num1 mod count)=0) and ((num2 mod count)=0) = false do
                                                count:= count - 1 ;
                                                writeln( count );
                                end
                        else
                                begin
                                        count := num1;
                                        while ((num2 mod count)=0) and ((num1 mod count)=0) = false do
                                                count:= count - 1 ;
                                                writeln( count );
                                end;

        end.
Program Geburtsjahr;
        var
                jahr: integer; res: real; age: integer;
        Begin
                write( ' Gib dein Geburtsjahr: ' );
                readln ( jahr );
                if jahr>0 then
                        Begin
                                res := jahr;
                                age := 2020 - jahr;
                                res := res * 2;
                                res := res + 5;
                                res := res * 50;
                                res := res + age;
                                res := res - 250;
                                res := res / 100;
                                writeln('Die Zahl ist ', res:0:2 );
                        End
                else
                        writeln( ' Das Jahr existiert nicht! ')
        End.
program Gerade;
        var
                sum, num: longint;
        begin
                sum := 0;
                num :=1;
                repeat
                        begin
                                if (num mod 2)=0 then
                                        sum := sum + num
                                else
                                        write();
                        num := num + 1;
                        end
                until num = 201;
                writeln ( sum , ' ist die SUmme der ersten 100 geraden zahlen.')
        End.
program Gleichung;
	var 
		a, b, c, d, wurzel1, wurzel2 : real;
	begin
		writeln('Geben Sie die Koeffizienten einer queadratischen Gleichung ein: ');
		read( a, b, c);
		d := ((b*b) - (4*a*c));

		if (d<0) then
			begin 
				writeln('Die wurzel dieser Gleichugn sind imaginär ( nicht möglich sozusagen )');
			end
		else
			begin
				wurzel1 := (((-b)+(sqrt(d)))/(2*a));
				wurzel2 := (((-b)-(sqrt(d)))/(2*a));
			end;
	
		if ( wurzel1 = wurzel2 ) then
			writeln('Die wurzel dieser Gleichung sing gleich und sie betragen ', wurzel1:0:2)
		else 
			writeln('Die Wurzel dieser Gleichung sind ', wurzel1:0:2, ' und ', wurzel2:0:2);
end.
program kgv;
        var
                x,y, cou : longint;
        begin

                writeln('Geben Sie eine Nummer ein: ');
                read( x );
                writeln('Geben Sie zweite Nummer ein: ');
                read( y );

                if x<y then
                        cou := x
                else
                        cou := y;

                while ((cou mod x)=0) and ((cou mod y)=0) <> true do
                        cou := cou + 1;
                writeln('');
                writeln( cou, ' ist KGV von ', x ,' und ', y);
        end.
program maxmin;
	var
		n, i, min, max, x, span, lastcount: integer;
		things : real;
	begin
		write('Geben Sie n ein: ');
		read( n );

		writeln('Jetzt geben Sie n Nummern ein.');
		for i:=1 to n do 
			begin
				read( x );
				lastcount := x;

				if (i=1) then
					begin
						min := lastcount;
						max := lastcount;
					end;
				if ((i>1) and (x>max))=True then
					max := x;
				if ((i>1) and (x<min))=True then
					min := x;
			end;
			
		writeln('Spannweite :', max-min);
		span := max+min;
		things := span/2;
		writeln('Average :', things:0:2);
				
		writeln('max ', max);
		writeln('min ', min);
end.
program primezahl;
        var
                num, did, count: longint;
        begin
                writeln('Geben Sie die Nummer ein, behalten Sie bitte im Kopf, dass wenn eine negative Zahl gegeben wird, wird der Betrag von der Zahl zur Prüfung genommen.');
                readln( num );
                num := abs( num );
                did := 1;
                count := 0;
                if (num <> 0) then
                        begin
                                while (did < num) do
                                        begin
                                                if (num mod did)=0 then
                                                        count := count + 1
                                                else;
                                                did := did + 1
                                        end;
                        if ((count = 2) or (count > 2)) then
                                writeln( 'Die gegebene Zahl ist eine Primzahl.')
                        else
                                writeln( 'Die gegebene Zahl ist keine Primzahl.')
                        end
                else
                        writeln('Die zahl ist null.');
                end.
program sterne;
        var
                x, i, starcount, linecount:longint;
        begin
                writeln(' Geben Sie eine Nummer von Sternen.');
                read( starcount );
                linecount := starcount;
                if (starcount > -1) then
                        begin
                                while (linecount>0) do
                                        begin
                                                for i:=1 to starcount do
                                                        begin
                                                                write('*');
                                                        end;
                                        linecount := linecount - 1;
                                        starcount := starcount - 1;
                                        writeln('');
                                        end;
                        end
                else
                        writeln('Die Nummer muss posiriv sein.');
end.
program summe;
        var
                num, i, sum: longint;
        begin
                sum := 0;
                writeln( ' Geben Sie eine Zahl ein: ' );
                read ( num );
                for i:=1 to num do
                        sum := sum + i;
                writeln ( sum );
        end.
Program Test;
        var
        z1,z2, sum, sumin, diff, diffin, mult, multin, quoin, quo: Integer;
        Begin
        writeln('Das ist ein Test um Ihre Ergebnisse zu testen. Viel Glück');

        writeln('Geben Sie erste Nummer ein: ');
        readln( z1 );

        writeln('Geben Sie Nummer ein: ');
        readln( z2 );

        writeln('Geben Sie Summe ein: ');
        readln( sumin );

        writeln('Geben Sie Differenz ein: ');
        readln( diffin );

        writeln('Geben Sie Multiplication ein: ');
        readln( multin );

        if z2<>0 then
                begin
                        writeln('Geben Sie Quotient ein: ');
                        readln( quoin );
                end
        else
                writeln( ' Division mit 0 nicht möglich ');

        sum := z1+z2;
        diff := z1-z2;
        mult := z1*z2;
        quo := z1 div z2;

        if sumin=sum then
                writeln('Summe ', sum, ' ist richtig')
        else
                writeln('Die Summe ist falsch. Richtige Summe ist ', sum);


        if diffin=diff then
                writeln('Differenz ', diff , ' ist richtig')
        else
                writeln('Die Differenz ist falsch. Richtige Summe ist ', diff);


        if multin=mult then
                writeln('Multiplication ', mult, ' ist richtig')
        else
                writeln('Die Multiplication ist falsch.Die Richtige Summe ist ', mult);


        if quoin=quo then
                writeln('Quotient ', quo, ' ist richtig')
        else
                writeln('Die Division ist falsch. Richtige Division ist ', quo);
End.
program Uhr;
         var
               time: Longint; hour: integer; minutes: integer; timesecinbet: integer; timesec: integer;
         Begin
                write('Gib die Zeit in Sekunden ein: ');
                readln ( time );
                hour := time div 3600;
                timesecinbet := time - ( hour * 3600 );
                minutes := timesecinbet div 60;
                timesec := timesecinbet - ( minutes * 60 );
                timesec := timesec div 1;
                if hour<10 then
                        write('0', hour)
                else
                        write( hour );
        write( ':' );
                if minutes<10 then
                        write('0', minutes )
                else
                        write( minutes );
        write( ':' );
                if timesec<10 then
                        writeln('0', timesec )
                else
                        writeln ( timesec, ' ' );
         End.
program Ungerade; 
        var 
                sum, num: longint;
        begin 
                sum := 0;
                num :=1;
                repeat 
                        begin
                                if (num mod 2)=1 then
                                        sum := sum + num
                                else 
                                        write();
                        num := num + 1;
                        end
                until num = 200;
                writeln ( sum , ' ist die SUmme der ersten 100 ungeraden zahlen.')
        End.

program vector;
        var
		x1, x2, y1, y2, z1, z2, sprod, vprodx, vprody, vprodz : real;

        begin
                write('Geben Sie ersten Vector: ');
                readln( x1, y1, z1);
                write('Geben Sie zweiten Vector: ');
                readln( x2, y2, z2);

                sprod := ( x1*x2 + y1*y2 + z1*z2);
		writeln('S Produkt ist ', sprod:0:2);

		vprodx := (( y1 * z2 ) - ( y2 - z1));
		vprody := (( z1 * x2 ) - ( z2 - x1));
		vprodz := (( x1 * y2 ) - ( x2 - y1));
		writeln('Vektor Produkt ist ', vprodx:0:2, 'i+', vprody:0:2, 'j+', vprodz:0:2, 'k');
        end.

