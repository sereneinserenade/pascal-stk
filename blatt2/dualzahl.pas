
Program dualzahl;

Var 
  num,i,count: longint;
  a: array[1..100] Of Integer;
Begin
  writeln('Geben Sie eine positive Zahl ein: ');
  readln(num);
  count := 0;
  If num>=0 Then
    Begin
      If num=1 Then
        writeln('Die Dualzahl von gegebener Dualzahl ist 1')
      Else
        Begin
          Repeat
            count := count+1;
            a[count] := num Mod 2;
            num := num Div 2;
          Until num=1;
          a[count+1] := 1;
          writeln('Die Dualzahl von gegebener Dualzahl ist ');
          For i:=count+1 Downto 1 Do
            write(a[i]);
        End
    End
  Else
    writeln('FEHLER!!');
  writeln;
End.
