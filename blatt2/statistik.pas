program statistik;
	var
		ar: array[1..100] of longint;
		b, i, ac, n, j : longint;
		medi, sum, mid, squm, sab : real;
	begin
		write('Wie viele Zahlen möchten Sie eingeben ?');
		read(n);

		writeln('Geben Sie jetzt die Zahlen ein.');

		ac := 1;
		for i:=1 to n do
			begin
				write(i,'>');
				read(ar[i]);
				writeln;
			end;

		for j:=1 to n do
			begin
				for i:=1 to n-1 do
					begin 
						b := ar[i];
						if (ar[i]>ar[i+1])  then
							begin
								ar[i] := ar[i+1];
								ar[i+1] := b;
							end;
					end;
			end;

		sum := 0;
		for i:=1 to n do
			begin 
				sum := sum + ar[i];
			end;
		mid := sum/n;

		writeln('Der Mittelwert ist : ', mid:0:1);

		if ((n mod 2) = 0) then
			medi := ( (ar[(n+1) div 2] + ar[( (n+1) div 2 ) + 1]) / 2 )
		else
			medi := ar[(n+1) div 2];
		writeln('Die Medien Zahl ist : ', medi:0:2);

		squm := 0;
		for i:=1 to n do 
			begin
				squm := squm + sqr(ar[i] - mid);
			end;
		sab := sqrt( squm/n );

		writeln('Die Standartabweichung ist : ', sab:0:2);

	writeln;
end.
