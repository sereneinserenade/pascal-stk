program dezimalzahl;
	var
		n, modu, count, sum, pot: longint;
		arr: Array[1..100] of integer;
		i: integer;
	begin
		write('Geben Sie eine 8-Bit Dualzahl ein: ');
		read(n);
		
		if n<0 then
			writeln('Keine Dualzahl')
		else 
			begin
				count:=8;
				while count>0 do
					begin
						modu := n mod 10;

						if (modu <> 0) and (modu <> 1) then
							begin
								writeln('Keine Dualzahl');
								break;
							end
						else
							begin	
								arr[count] := modu;
								count := count - 1;
								n := n div 10;
							end;

					end;
				
				if (n=0) and (count=0) then		
					begin
						write('Die Dezimalzahl ist: ');
						sum := 0;
						pot := 128;

						for i:=1 to 8 do 
							begin
								sum := sum + ( pot * arr[i]);
								pot := pot div 2;
							end;
							write(sum);
					end
				else 
					begin
						while count = 0 do
							writeln('Das war mehr als 1 Byte.');
					end;
			end;
			writeln;
end.
