program zahl;
	var
	n, a, i, b : longint;
	begin
	writeln('Geben Sie eine natürliche Zahl ein : ');
	read(n);

	writeln('Die Zahlenpaare sind: ');
	for i:=1 to n-1 do
		begin
			a := i;
			b := n - a;
			write('(', a, ',', b, '), ');
		end;
	writeln;
end.
